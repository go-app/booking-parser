package model

import (
	"fmt"
	"log"
)

// Account is a ReadFolder with matching postings
type Account struct {
	ReadFolder     *ReadFolder
	BookingPeriods []*BookingPeriod
}

// BookingPeriod are Bookings of a time intervall
type BookingPeriod struct {
	ReadFile    *ReadFile
	PosBookings []*Booking
	NegBookings []*Booking
	Violations  *ViolationArray
}

// Booking match between ReadLine and Posting from config (expected-postings)
type Booking struct {
	ReadLine *ReadLine
	Posting  *Posting
}

// Fill insert all info of ReadFolder in Account incl periods and bookings
func (account *Account) Fill(readFolder *ReadFolder) {
	account.ReadFolder = readFolder
	account.BookingPeriods = make([]*BookingPeriod, 0, len(readFolder.ReadFiles))

	for _, readFile := range readFolder.ReadFiles {
		period := new(BookingPeriod)
		period.fill(readFile, readFolder.Format)
		account.BookingPeriods = append(account.BookingPeriods, period)
	}
}

func (period *BookingPeriod) fill(readFile *ReadFile, format *CsvFormat) {
	period.PosBookings = make([]*Booking, 0, len(readFile.PosLines))
	period.NegBookings = make([]*Booking, 0, len(readFile.NegLines))
	period.Violations = &ViolationArray{}
	period.ReadFile = readFile

	for _, entry := range readFile.PosLines {
		period.insertBooking(entry, format, readFile.FilePath)
	}

	for _, entry := range readFile.NegLines {
		period.insertBooking(entry, format, readFile.FilePath)
	}
}

func (period *BookingPeriod) insertBooking(readLine *ReadLine, format *CsvFormat, filePath string) {
	posting := format.GetPosting(readLine.Names)
	if readLine.Number == 0 {
		msg := fmt.Sprint(" \n *** File: ", filePath, " Missing number for line:", readLine.LineNumber, " Names:", readLine.Names)
		log.Println(msg)
		period.Violations.Add(msg)
	} else if posting == nil {
		msg := fmt.Sprint(" \n *** File: ", filePath, " Missing key for line:", readLine.LineNumber, " Names:", readLine.Names)
		log.Println(msg)
		period.Violations.Add(msg)
	} else {
		period.addBooking(readLine, posting)
	}
}

func (period *BookingPeriod) addBooking(readLine *ReadLine, posting *Posting) {
	booking := &Booking{
		ReadLine: readLine,
		Posting:  posting,
	}
	if readLine.Number < 0 {
		period.NegBookings = append(period.NegBookings, booking)
	} else {
		period.PosBookings = append(period.PosBookings, booking)
	}
}
