package model

import (
	"time"
)

// AccountSum is the sum of bookings mapped by posting.key
type AccountSum struct {
	Account           *Account
	BookingPeriodSums []*BookingPeriodSum
}

// BookingPeriodSum is the sum of bookings maaped by posting.key for one period / file
type BookingPeriodSum struct {
	BookingPeriod  *BookingPeriod
	PosKeyMatchMap map[string]*BookingPeriodMatch
	NegKeyMatchMap map[string]*BookingPeriodMatch
	FirstDate      time.Time
	LastDate       time.Time
	SumPosBookings float64
	SumNegBookings float64
}

// BookingPeriodMatch is matching between all bookings / file lines with same posting.key
type BookingPeriodMatch struct {
	Posting  *Posting
	Bookings []*Booking
	Sum      float64
}

func (accountSum *AccountSum) Fill(account *Account) {
	accountSum.Account = account
	accountSum.BookingPeriodSums = make([]*BookingPeriodSum, 0, len(account.BookingPeriods))

	for _, period := range account.BookingPeriods {
		periodSum := new(BookingPeriodSum)
		periodSum.fill(period)
		accountSum.BookingPeriodSums = append(accountSum.BookingPeriodSums, periodSum)
	}
}

func (periodSum *BookingPeriodSum) fill(period *BookingPeriod) {
	periodSum.PosKeyMatchMap = map[string]*BookingPeriodMatch{}
	periodSum.NegKeyMatchMap = map[string]*BookingPeriodMatch{}

	periodSum.BookingPeriod = period
	periodSum.SumPosBookings = 0
	periodSum.SumNegBookings = 0

	for _, booking := range period.PosBookings {
		addEntry(periodSum.PosKeyMatchMap, booking)
		periodSum.SumPosBookings += booking.ReadLine.Number
	}

	for _, booking := range period.NegBookings {
		addEntry(periodSum.NegKeyMatchMap, booking)
		periodSum.SumNegBookings += booking.ReadLine.Number
	}

	periodSum.FirstDate, periodSum.LastDate = findFirstLastDate(period)
}

func addEntry(keyEntryMap map[string]*BookingPeriodMatch, booking *Booking) {
	value, ok := keyEntryMap[booking.Posting.Key]
	if ok {
		value.Sum += booking.ReadLine.Number
		value.Bookings = append(value.Bookings, booking)
	} else {
		value := &BookingPeriodMatch{
			Posting:  booking.Posting,
			Bookings: make([]*Booking, 0, 5),
			Sum:      0,
		}
		value.Sum += booking.ReadLine.Number
		value.Bookings = append(value.Bookings, booking)
		keyEntryMap[booking.Posting.Key] = value
	}
}

func sumPosBookings(period *BookingPeriod) float64 {
	return sumBookings(period.PosBookings)
}

func sumNegBookings(period *BookingPeriod) float64 {
	return sumBookings(period.NegBookings)
}

func sumBookings(bookings []*Booking) float64 {
	sum := 0.0
	for _, b := range bookings {
		sum += b.ReadLine.Number
	}
	return sum
}

func findFirstLastDate(period *BookingPeriod) (time.Time, time.Time) {

	var firstDate time.Time
	var lastDate time.Time

	if len(period.NegBookings) > 0 {
		current := period.NegBookings[0].ReadLine.Date
		firstDate = current
		lastDate = current
	} else if len(period.PosBookings) > 0 {
		current := period.PosBookings[0].ReadLine.Date
		firstDate = current
		lastDate = current
	}

	for _, b := range period.NegBookings {
		current := b.ReadLine.Date
		if firstDate.After(current) {
			firstDate = current
		}
		if lastDate.Before(current) {
			lastDate = current
		}
	}

	for _, b := range period.PosBookings {
		current := b.ReadLine.Date
		if firstDate.After(current) {
			firstDate = current
		}
		if lastDate.Before(current) {
			lastDate = current
		}
	}
	return firstDate, lastDate
}
