package model

import "fmt"

// ViolationArray is array of vialtions
type ViolationArray struct {
	violations []Violation
}

// Violation is for example a booking with missing expected posting.key
// or a amount higher then a limit
type Violation struct {
	message string
}

func (vioArray *ViolationArray) IsEmpty() bool {
	return len(vioArray.violations) == 0
}

func (vioArray *ViolationArray) Size() int {
	return len(vioArray.violations)
}

func (vioArray *ViolationArray) ToString() string {
	if vioArray.IsEmpty() {
		return ""
	}
	result := ""
	for _, violation := range vioArray.violations {
		result += fmt.Sprintln(violation.message)
	}
	return result
}

func (vioArray *ViolationArray) Add(message string) {
	if vioArray.violations == nil {
		vioArray.violations = make([]Violation, 0, 3)
	}
	vioArray.violations = append(vioArray.violations, Violation{
		message: message,
	})
}
