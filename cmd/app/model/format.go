package model

import (
	"strings"
)

type CsvFormat struct {
	Parser struct {
		Delimiter           string `yaml:"delimiter"`
		NewBalanceIndicator string `yaml:"new-balance-indicator"`
		OldBalanceIndicator string `yaml:"old-balance-indicator"`
		LineFirstBooking    int    `yaml:"line-first-booking"`
		NumbersIndex        []int  `yaml:"numbers-index"`
		NamesIndex          []int  `yaml:"names-index"`
		DateIndex           int    `yaml:"date-index"`
	} `yaml:"parser"`
	Meta struct {
		AccountName string `yaml:"acccount-name"`
	} `yaml:"meta"`
	ExpectedPostings []Posting `yaml:"expected-postings"`
}

type Posting struct {
	Names             []string `yaml:"names"`
	Key               string   `yaml:"key"`
	LimitPerPost      float64  `yaml:"limit-per-post"`
	LimitSum          float64  `yaml:"limit-sum"`
	ExcludeLimitDates []string `yaml:"exclude-limit-dates"`
}

func (config *CsvFormat) getPosting(name string) *Posting {
	for _, posting := range config.ExpectedPostings {
		for _, postName := range posting.Names {
			if strings.Contains(name, postName) {
				return &posting
			}
		}
	}
	return nil
}

func (config *CsvFormat) GetPosting(names []string) *Posting {
	for _, str := range names {
		posting := config.getPosting(str)
		if posting != nil {
			return posting
		}
	}
	return nil
}
