package model

type CsvConfig struct {
	Csv struct {
		Folders []string `yaml:"folders"`
	} `yaml:"csv"`
	Output struct {
		FilePath           string `yaml:"path"`
		WriteEmptyBookings bool   `yaml:"write-empty-bookings"`
	} `yaml:"output"`
}
