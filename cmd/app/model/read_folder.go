package model

import (
	"time"
)

// ReadFolder is a read in directory with amount of csv files
type ReadFolder struct {
	Name      string
	ReadFiles []*ReadFile
	Format    *CsvFormat
}

// ReadFile are read in lines for a time intervall (one csv file)
type ReadFile struct {
	Name       string
	FilePath   string
	PosLines   []*ReadLine
	NegLines   []*ReadLine
	NewBalance float64
	OldBalance float64
}

// ReadLine is a line of a csv means a booking
type ReadLine struct {
	Names      []string
	Number     float64
	Date       time.Time
	LineNumber int
}

// Init creates a new ReadFolder
func (folder *ReadFolder) Init(name string, format *CsvFormat) {
	folder.ReadFiles = make([]*ReadFile, 0, 10)
	folder.Format = format
	folder.Name = name
}

func (folder *ReadFolder) AddFile(readFile *ReadFile) {
	folder.ReadFiles = append(folder.ReadFiles, readFile)
}

func (readFile *ReadFile) Init(name string, filePath string) {
	readFile.Name = name
	readFile.FilePath = filePath
	readFile.PosLines = make([]*ReadLine, 0, 10)
	readFile.NegLines = make([]*ReadLine, 0, 10)
}

func (file *ReadFile) AddLine(names []string, number float64, date time.Time, lineNumber int) {
	line := &ReadLine{
		Names:      names,
		Number:     number,
		Date:       date,
		LineNumber: lineNumber,
	}
	if number < 0 {
		file.NegLines = append(file.NegLines, line)
	} else {
		file.PosLines = append(file.PosLines, line)
	}
}
