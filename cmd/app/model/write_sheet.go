package model

import (
	"sort"
	"time"
)

// WriteSheet is a sheet to write out with collection of periods/columns
type WriteSheet struct {
	WriteColumns []*WriteColumn
}

// WriteColumn are sum bookings for one period/column over all accounts mapped by name (file name)
type WriteColumn struct {
	PeriodName     string
	WriteBlocks    []*WriteBlock
	FirstDate      time.Time
	LastDate       time.Time
	SumPosBookings float64
	SumNegBookings float64
}

// WriteBlock are all bookings for one account in one period
type WriteBlock struct {
	BookingPeriodSum *BookingPeriodSum
	AccountSum       *AccountSum
	AccountName      string
}

// Fill insert all info of ReadFolder in Account incl periods and bookings
func (sheet *WriteSheet) Fill(accountSums []*AccountSum) {

	periodBookingsMap := createPeriodBookingMap(accountSums)
	sortedPeriodNames := sortedKeys(periodBookingsMap)

	sheet.WriteColumns = make([]*WriteColumn, 0, len(periodBookingsMap))

	for _, periodName := range sortedPeriodNames {
		writeBlockArray := periodBookingsMap[periodName]
		writeColumn := new(WriteColumn)
		writeColumn.PeriodName = periodName
		writeColumn.WriteBlocks = writeBlockArray
		writeColumn.SumPosBookings, writeColumn.SumNegBookings = calcPosNegSum(writeBlockArray)

		sheet.WriteColumns = append(sheet.WriteColumns, writeColumn)
	}
}

func sortedKeys(m map[string][]*WriteBlock) []string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}

func calcPosNegSum(writeBlocks []*WriteBlock) (float64, float64) {
	sumPos := 0.0
	sumNeg := 0.0
	for _, block := range writeBlocks {
		sumPos += block.BookingPeriodSum.SumPosBookings
		sumNeg += block.BookingPeriodSum.SumNegBookings
	}
	return sumPos, sumNeg
}

func createPeriodBookingMap(accountSums []*AccountSum) map[string][]*WriteBlock {

	periodBookingsMap := map[string][]*WriteBlock{}

	for _, accountSum := range accountSums {
		for _, periodSum := range accountSum.BookingPeriodSums {
			fileName := periodSum.BookingPeriod.ReadFile.Name

			array, ok := periodBookingsMap[fileName]
			if !ok {
				array = make([]*WriteBlock, 0, 12)
			}

			block := new(WriteBlock)
			block.AccountSum = accountSum
			block.BookingPeriodSum = periodSum
			block.AccountName = accountSum.Account.ReadFolder.Format.Meta.AccountName

			array = append(array, block)
			periodBookingsMap[fileName] = array
		}
	}
	return periodBookingsMap
}
