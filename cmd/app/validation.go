package app

import (
	"booking-parser/cmd/app/io"
	"booking-parser/cmd/app/model"
	"fmt"
	"log"
	"math"
)

const (
	// it is possible to exceed the limit by this number
	LIMIT_BUFFER = 5
)

// Validate checks limits and sum versus balances
// if found mistakees it is written in violations
func Validate(periodSum *model.BookingPeriodSum) {

	validateLimits(periodSum.BookingPeriod)

	for _, match := range periodSum.PosKeyMatchMap {
		validateLimitSum(match, periodSum.BookingPeriod.Violations)
	}
	for _, match := range periodSum.NegKeyMatchMap {
		validateLimitSum(match, periodSum.BookingPeriod.Violations)
	}
	validateBalanaceDiv(periodSum, periodSum.BookingPeriod.Violations)
}

func validateLimits(period *model.BookingPeriod) {
	for _, booking := range period.NegBookings {
		validateLimit(booking, period.Violations)
	}
	for _, booking := range period.PosBookings {
		validateLimit(booking, period.Violations)
	}
}

func validateLimit(booking *model.Booking, violations *model.ViolationArray) {
	if booking.Posting.LimitPerPost <= 0 {
		return
	}

	if isLimitExclude(booking) {
		return
	}

	if math.Abs(booking.ReadLine.Number) > LIMIT_BUFFER+booking.Posting.LimitPerPost {
		msg := fmt.Sprint("Booking is over expected limit:", booking.ReadLine, "\n\t", booking.ReadLine.Number)
		log.Println(msg)
		violations.Add(msg)
	}
}

func isLimitExclude(booking *model.Booking) bool {
	for _, excludeDate := range booking.Posting.ExcludeLimitDates {
		date := io.ParseDate(excludeDate)
		if date == booking.ReadLine.Date {
			return true
		}
	}
	return false
}

func validateLimitSum(match *model.BookingPeriodMatch, violations *model.ViolationArray) {
	if match.Posting.LimitSum <= 0 {
		return
	}

	sum := 0.0
	for _, booking := range match.Bookings {
		if isLimitExclude(booking) {
			continue
		}
		sum += booking.ReadLine.Number
	}

	if math.Abs(sum) > LIMIT_BUFFER+match.Posting.LimitSum {
		msg := fmt.Sprint("Sum bookings is over expected limit:", match.Posting.Key, "\n\t", match.Sum)
		log.Println(msg)
		violations.Add(msg)
	}
}

func validateBalanaceDiv(periodSum *model.BookingPeriodSum, violations *model.ViolationArray) {

	// guess it was not possible to find balances of account
	if periodSum.BookingPeriod.ReadFile.NewBalance == 0 || periodSum.BookingPeriod.ReadFile.OldBalance == 0 {
		return
	}

	divSums := periodSum.SumNegBookings + periodSum.SumPosBookings
	divBalanaces := periodSum.BookingPeriod.ReadFile.NewBalance - periodSum.BookingPeriod.ReadFile.OldBalance
	if math.Abs(divSums-divBalanaces) > LIMIT_BUFFER {
		msg := fmt.Sprint("Sum bookings and difference of balances does not match:", divSums, divBalanaces)
		log.Println(msg)
		violations.Add(msg)
	}
}
