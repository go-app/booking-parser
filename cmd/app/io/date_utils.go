package io

import (
	"log"
	"time"
)

// ParseDate can parse format 'dd.mm.yyyy'
func ParseDate(string string) time.Time {
	date, err := time.Parse("02.01.2006", string)
	if err != nil {
		log.Fatal(err)
	}
	return date
}

// FormatDate create string of date in format 'dd.mm.yyyy'
func FormatDate(date time.Time) string {
	printFormat := "02.01.2006"
	return date.Format(printFormat)
}
