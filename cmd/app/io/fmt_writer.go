package io

import (
	model "booking-parser/cmd/app/model"
	"fmt"
	"sort"
)

func WritePeriod(period *model.BookingPeriod) {
	posBooks := formatBooking(period.PosBookings)
	fmt.Println("\n=======Positive bookings=========", posBooks)
	fmt.Println("==================================")

	negBooks := formatBooking(period.NegBookings)
	fmt.Println("\n=======Negative bookings========", negBooks)
	fmt.Println("==================================")

	fmt.Println("\n=======Results==================")
	fmt.Println(formatNum("New Balance", period.ReadFile.NewBalance))
	fmt.Println(formatNum("Old Balance", period.ReadFile.OldBalance))

	divBalanaces := period.ReadFile.NewBalance - period.ReadFile.OldBalance
	fmt.Println(formatNum("Div of balances", divBalanaces))
}

func WritePeriodSumArray(sumArray []*model.AccountSum, config *model.CsvConfig) {
	violations := 0
	for _, sum := range sumArray {
		for _, period := range sum.BookingPeriodSums {
			violations += period.BookingPeriod.Violations.Size()
			WritePeriodSum(period, sum.Account.ReadFolder.Format.Meta.AccountName)
		}
	}
	fmt.Println("\n *** Total violations:", violations, "***")
}

func WritePeriodSum(periodSum *model.BookingPeriodSum, accountName string) {

	fmt.Println("\n======= Result", accountName, periodSum.BookingPeriod.ReadFile.Name, "==================")
	divSums := periodSum.SumNegBookings + periodSum.SumPosBookings

	if periodSum.BookingPeriod.ReadFile.NewBalance != 0 || periodSum.BookingPeriod.ReadFile.OldBalance != 0 {
		fmt.Println(formatNum("New Balance", periodSum.BookingPeriod.ReadFile.NewBalance))
		fmt.Println(formatNum("Old Balance", periodSum.BookingPeriod.ReadFile.OldBalance))
		divBalanaces := periodSum.BookingPeriod.ReadFile.NewBalance - periodSum.BookingPeriod.ReadFile.OldBalance
		fmt.Println(formatNum("Div of balances", divBalanaces))
	}

	fmt.Println(formatNum("Div of sum", divSums))

	fmt.Println("First booking:", FormatDate(periodSum.FirstDate))
	fmt.Println("Last booking:", FormatDate(periodSum.LastDate))

	if !periodSum.BookingPeriod.Violations.IsEmpty() {
		fmt.Print(periodSum.BookingPeriod.Violations.ToString())
	} else {
		fmt.Println("No violations")
	}
}

func writePeriodSumDetails(periodSum *model.BookingPeriodSum) {
	posBooks := formatMapSorted(periodSum.PosKeyMatchMap)
	fmt.Println("\n=======Positive booking sum=========", posBooks)
	fmt.Println(formatNum("Sum positive bookings", periodSum.SumPosBookings))
	fmt.Println("==================================")

	negBooks := formatMapSorted(periodSum.NegKeyMatchMap)
	fmt.Println("\n=======Negative booking sum========", negBooks)
	fmt.Println(formatNum("Sum negative bookings", periodSum.SumNegBookings))
	fmt.Println("==================================")
}

func formatBooking(bookings []*model.Booking) string {
	result := "\n"
	for _, b := range bookings {
		result += fmt.Sprintln("Date:", FormatDate(b.ReadLine.Date), " Key:", b.Posting.Key, formatNum(" Number", b.ReadLine.Number), " Line: ", b.ReadLine.LineNumber)
	}
	return result
}

func formatMapSorted(m map[string]*model.BookingPeriodMatch) string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	result := "\n"
	for _, k := range keys {
		result += fmt.Sprintln(formatNum(k, m[k].Sum))
	}
	return result
}

func formatNum(name string, num float64) string {
	result := fmt.Sprintf("%v: %.2f", name, num)
	return result
}
