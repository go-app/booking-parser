package io

import (
	model "booking-parser/cmd/app/model"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

func CreateFormatFromFolder(folderPath string) *model.CsvFormat {

	files, err := ioutil.ReadDir(folderPath)
	if err != nil {
		log.Fatal(err)
	}

	formatFile := ""
	for _, file := range files {
		if file.Name() == "format.yaml" {
			formatFile = fmt.Sprint(folderPath, "/", file.Name())
			break
		}
	}

	if len(formatFile) == 0 {
		log.Fatal("Missing 'format.yaml' file in folder", folderPath)
	}

	return CreateFormat(formatFile)
}

func CreateFormat(filePath string) *model.CsvFormat {
	format := new(model.CsvFormat)
	parseYaml(format, filePath)
	return format
}

func CreateConfig(filePath string) *model.CsvConfig {
	config := new(model.CsvConfig)
	parseYaml(config, filePath)
	return config
}

func parseYaml(config interface{}, filePath string) {
	log.Println("Open", filePath)
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	log.Println("Parse", filePath)

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(config)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Done", filePath)
}
