package io

import (
	model "booking-parser/cmd/app/model"
	"bufio"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func ParseCsvFile(name string, filePath string, format *model.CsvFormat) *model.ReadFile {

	if !fileExists(filePath) {
		log.Fatalln("No file found: ", filePath)
	}

	if !fileIsCSV(filePath) {
		log.Fatalln("File is not *.csv: ", filePath)
	}

	csvFile, error := os.Open(filePath)
	if error != nil {
		log.Fatal(error)
	}

	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.Comma = []rune(format.Parser.Delimiter)[0]

	readFile := new(model.ReadFile)
	readFile.Init(name, filePath)

	lineNum := 0
	relevantLines := 0
	for ; ; lineNum++ {

		line, error := reader.Read()
		if error == io.EOF {
			break
		}

		if ok, newBalance := getNewBalance(line, format); ok {
			readFile.NewBalance = newBalance
			continue
		}

		if ok, oldBalance := getOldBalance(line, format); ok {
			readFile.OldBalance = oldBalance
			continue
		}

		if lineNum < format.Parser.LineFirstBooking {
			continue
		}

		_, max := findMinAndMax(format.Parser.NumbersIndex)
		if len(line) <= max {
			continue
		}

		relevantLines++
		number := getNumber(line, format)
		names := getNames(line, format)
		date, _ := getDate(line, format)

		readFile.AddLine(names, number, date, lineNum)
	}

	return readFile
}

func getNewBalance(line []string, format *model.CsvFormat) (bool, float64) {
	return getBalance(line, format.Parser.NewBalanceIndicator)
}

func getOldBalance(line []string, format *model.CsvFormat) (bool, float64) {
	return getBalance(line, format.Parser.OldBalanceIndicator)
}

func getBalance(line []string, indicator string) (bool, float64) {
	for _, str := range line {
		if len(str) <= 0 {
			continue
		}
		if str == indicator {
			num := getFirstNumberOfLine(line)
			if num != 0 {
				return true, num
			}
		}
	}
	return false, 0
}

func getNames(line []string, format *model.CsvFormat) []string {
	result := make([]string, 0, 3)
	for _, v := range format.Parser.NamesIndex {
		str := line[v]
		if len(str) <= 0 {
			continue
		}
		result = append(result, str)
	}
	return result
}

func getNumber(line []string, format *model.CsvFormat) float64 {

	for _, v := range format.Parser.NumbersIndex {
		numString := line[v]
		number := parseNumber(numString)
		if number != 0 {
			return number
		}
	}
	return 0
}

func parseNumber(numString string) float64 {
	if len(numString) <= 0 {
		return 0
	}
	if !strings.Contains(numString, ",") {
		return 0
	}
	numString = strings.Replace(numString, ".", "", 1)
	numString = strings.Replace(numString, ",", ".", 1)
	numString = strings.Replace(numString, "EUR", "", 1)
	numString = strings.TrimSpace(numString)
	num, err := strconv.ParseFloat(numString, 32)
	if err == nil {
		return num
	}
	return 0
}

func getFirstNumberOfLine(line []string) float64 {
	for _, str := range line {
		number := parseNumber(str)
		if number != 0 {
			return number
		}
	}
	return 0
}

func getDate(line []string, format *model.CsvFormat) (time.Time, error) {
	var result time.Time
	if format.Parser.DateIndex < len(line) {
		date := line[format.Parser.DateIndex]
		if strings.Count(date, ".") == 2 {
			return ParseDate(date), nil
		}
	}
	msg := fmt.Sprint("Cannot parse date in line:", line)
	return result, errors.New(msg)
}
