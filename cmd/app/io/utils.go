package io

import (
	"os"
	"strings"
)

// checks if a file exists and is not a directory
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func fileIsCSV(filename string) bool {
	return strings.HasSuffix(filename, ".csv")
}

func stringIsBlank(str string) bool {
	trim := strings.TrimSpace(str)
	return len(trim) == 0
}

func findMinAndMax(a []int) (min int, max int) {
	min = a[0]
	max = a[0]
	for _, value := range a {
		if value < min {
			min = value
		}
		if value > max {
			max = value
		}
	}
	return min, max
}
