package io

import (
	"booking-parser/cmd/app/model"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	// SEPERATOR seperate csv entries
	SEPERATOR string = ";"
	NEW_LINE         = "\n"
)

func combineColumns(blockLeft, blockRight string) string {

	if len(blockLeft) == 0 {
		return blockRight
	}
	if len(blockRight) == 0 {
		return blockLeft
	}

	leftSplit := strings.SplitN(blockLeft, NEW_LINE, -1)
	rightSplit := strings.SplitN(blockRight, NEW_LINE, -1)

	columnsLeft := len(strings.SplitN(leftSplit[0], SEPERATOR, -1))
	emptyLeftLine := ""
	for i := 0; i < columnsLeft-1; i++ {
		emptyLeftLine += ";"
	}

	maxRows := len(leftSplit)
	if maxRows < len(rightSplit) {
		maxRows = len(rightSplit)
	}

	result := ""
	for i := 0; i < maxRows; i++ {
		left := emptyLeftLine
		if i < len(leftSplit) {
			if len(leftSplit[i]) > 0 {
				left = leftSplit[i]
			}
		}
		right := csvf("", "")
		if i < len(rightSplit) {
			if len(rightSplit[i]) > 0 {
				right = rightSplit[i]
			}
		}
		result += fmt.Sprintln(left, right)
	}
	return result
}

func WriteCsvSheet(writeSheet *model.WriteSheet, writeEmptyBookings bool, outFilePath string) {

	csvToWrite := ""

	emptyLine := emptyLine()
	seperatorLine := seperatorLine()

	for _, column := range writeSheet.WriteColumns {

		columnToWrite := headerLine(column.PeriodName)

		for _, block := range column.WriteBlocks {
			posHeaderLine := posHeaderLine(block.AccountName)
			expectedPostings := block.AccountSum.Account.ReadFolder.Format.ExpectedPostings
			periodSum := block.BookingPeriodSum
			posLines := posBookingLines(periodSum, writeEmptyBookings, expectedPostings)

			negHeaderLine := negHeaderLine(block.AccountName)
			negLines := negBookingLines(periodSum, writeEmptyBookings, expectedPostings)

			diffLine := diffLine(periodSum)
			violationLine := violationLine(periodSum)
			columnToWrite = fmt.Sprintln(columnToWrite, posHeaderLine, posLines, emptyLine, negHeaderLine, negLines, emptyLine, diffLine, violationLine, seperatorLine)
		}
		totalSumLine := totalSumLine(column)
		columnToWrite = fmt.Sprint(columnToWrite, totalSumLine)

		csvToWrite = combineColumns(csvToWrite, columnToWrite)
		csvToWrite = addEmptyColumn(csvToWrite)
	}
	writeToFile(outFilePath, csvToWrite)
}

func addEmptyColumn(columnBefore string) string {
	return combineColumns(columnBefore, csvln(" ", " "))
}

func totalSumLine(writeColumn *model.WriteColumn) string {
	result := csvNumln("Total sum negative", writeColumn.SumNegBookings)
	result += csvNumln("Total sum positive", writeColumn.SumPosBookings)
	result += csvNumln("Total Diff", writeColumn.SumPosBookings+writeColumn.SumNegBookings)
	return result
}

func violationLine(periodSum *model.BookingPeriodSum) string {
	if periodSum.BookingPeriod.Violations.IsEmpty() {
		return csvln("", "")
	}
	return csvNumln("Violations", float64(periodSum.BookingPeriod.Violations.Size()))
}

func WriteCsvPeriodSumArray(periodSumArray []*model.BookingPeriodSum, writeEmptyBookings bool, outFilePath string, expectedPostings []model.Posting) {

	csvToWrite := ""

	for _, periodSum := range periodSumArray {

		headerLine := headerLine(periodSum.BookingPeriod.ReadFile.Name)

		emptyLine := emptyLine()
		posHeaderLine := posHeaderLine("")
		posLines := posBookingLines(periodSum, writeEmptyBookings, expectedPostings)

		negHeaderLine := negHeaderLine("")
		negLines := negBookingLines(periodSum, writeEmptyBookings, expectedPostings)

		diffLine := diffLine(periodSum)

		block := fmt.Sprint(headerLine, posHeaderLine, posLines, emptyLine, negHeaderLine, negLines, emptyLine, diffLine)

		csvToWrite = combineColumns(csvToWrite, block)
	}
	writeToFile(outFilePath, csvToWrite)
}

func diffLine(periodSum *model.BookingPeriodSum) string {
	result := emptyLine()
	if periodSum.BookingPeriod.ReadFile.NewBalance != 0 || periodSum.BookingPeriod.ReadFile.OldBalance != 0 {
		result = csvNumln("Old balance", periodSum.BookingPeriod.ReadFile.OldBalance)
		result += csvNumln("New balance", periodSum.BookingPeriod.ReadFile.NewBalance)
		result += csvNumln("Diff", periodSum.BookingPeriod.ReadFile.NewBalance-periodSum.BookingPeriod.ReadFile.OldBalance)
	}
	return result
}

func emptyLine() string {
	return csvln(" ", " ")
}

func seperatorLine() string {
	return csvln("~~~~~~~~~~", "~~~~~~~~~~")
}

func writeToFile(filePath string, contentArr ...string) {
	outFile, err := os.Create(filePath)
	if err != nil {
		log.Fatal(err)
	}

	defer outFile.Close()

	for _, content := range contentArr {
		outFile.WriteString(content)
	}
	outFile.Sync()
}

func negBookingLines(periodSum *model.BookingPeriodSum, writeEmptyBookings bool, expectedPostings []model.Posting) string {
	posEntryMap := func(periodSumArray *model.BookingPeriodSum, key string) (*model.BookingPeriodMatch, bool) {
		value, ok := periodSumArray.NegKeyMatchMap[key]
		return value, ok
	}
	return bookingLines(periodSum, writeEmptyBookings, expectedPostings, posEntryMap)
}

func posBookingLines(periodSum *model.BookingPeriodSum, writeEmptyBookings bool, expectedPostings []model.Posting) string {
	posEntryMap := func(periodSumArray *model.BookingPeriodSum, key string) (*model.BookingPeriodMatch, bool) {
		value, ok := periodSumArray.PosKeyMatchMap[key]
		return value, ok
	}
	return bookingLines(periodSum, writeEmptyBookings, expectedPostings, posEntryMap)
}

func bookingLines(periodSum *model.BookingPeriodSum, writeEmptyBookings bool, expectedPostings []model.Posting, entryMap func(periodSum *model.BookingPeriodSum, key string) (*model.BookingPeriodMatch, bool)) string {
	result := ""
	for _, posting := range expectedPostings {
		line := bookingLine(posting.Key, periodSum, writeEmptyBookings, entryMap)
		if len(line) > 0 {
			result += line
		}
	}
	return result
}

func bookingLine(key string, periodSum *model.BookingPeriodSum, writeEmptyBookings bool, entryMap func(periodSum *model.BookingPeriodSum, key string) (*model.BookingPeriodMatch, bool)) string {
	line := ""
	value, ok := entryMap(periodSum, key)
	if ok {
		line = csvNumln(key, value.Sum)
	} else if writeEmptyBookings {
		line = csvNumln(key, 0.0)
	}
	return line
}

func csvNumf(label string, num float64) string {
	return csvf(label, numf(num))
}

func csvf(label, value string) string {
	return fmt.Sprint(label, SEPERATOR, value, SEPERATOR, " ")
}

func numf(num float64) string {
	return fmt.Sprintf("%.2f", num)
}

func csvNumln(label string, num float64) string {
	return csvln(label, numf(num))
}

func csvln(label, value string) string {
	return fmt.Sprintln(label, SEPERATOR, value, SEPERATOR, " ")
}

func headerLine(fileName string) string {
	return csvln("Bookings of ", fileName)
}

func posHeaderLine(accountName string) string {
	label := fmt.Sprint("Positive ", accountName)
	return csvln(label, "Amount")
}

func negHeaderLine(accountName string) string {
	label := fmt.Sprint("Negative ", accountName)
	return csvln(label, "Amount")
}
