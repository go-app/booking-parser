package app

import (
	io "booking-parser/cmd/app/io"
	model "booking-parser/cmd/app/model"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func Run() []*model.AccountSum {

	var configPath string
	if len(os.Args) <= 1 {
		log.Fatalln("First parameter has to the path to a config.yaml")
	} else {
		configPath = os.Args[1]
	}
	return RunWithConfig(configPath)
}

func RunWithConfig(configPath string) []*model.AccountSum {

	config := io.CreateConfig(configPath)

	folderArray := parseFolder(config)

	accountArray := createAccounts(folderArray)

	sumArray := createAccountSums(accountArray)

	validateSums(sumArray)

	writeSums(sumArray, config)

	return sumArray
}

func writeSums(sumArray []*model.AccountSum, config *model.CsvConfig) {

	io.WritePeriodSumArray(sumArray, config)

	sheet := new(model.WriteSheet)
	sheet.Fill(sumArray)
	io.WriteCsvSheet(sheet, config.Output.WriteEmptyBookings, config.Output.FilePath)
}

func validateSums(sumArray []*model.AccountSum) {
	for _, sum := range sumArray {
		for _, period := range sum.BookingPeriodSums {
			Validate(period)
		}
	}
}

func createAccountSums(accountArray []*model.Account) []*model.AccountSum {

	sumArray := make([]*model.AccountSum, 0, len(accountArray))
	for _, account := range accountArray {
		sum := new(model.AccountSum)
		sum.Fill(account)
		sumArray = append(sumArray, sum)
	}
	return sumArray
}

func createAccounts(folderArray []*model.ReadFolder) []*model.Account {

	accountArray := make([]*model.Account, 0, len(folderArray))
	for _, folder := range folderArray {
		account := new(model.Account)
		account.Fill(folder)
		accountArray = append(accountArray, account)
	}
	return accountArray
}

func parseFolder(config *model.CsvConfig) []*model.ReadFolder {

	folderArray := make([]*model.ReadFolder, 0, len(config.Csv.Folders))

	for _, folderPath := range config.Csv.Folders {
		format := io.CreateFormatFromFolder(folderPath)

		readFolder := new(model.ReadFolder)
		readFolder.Init(folderPath, format)

		files, err := ioutil.ReadDir(folderPath)
		if err != nil {
			log.Fatal(err)
		}

		for _, file := range files {
			if !strings.HasSuffix(file.Name(), ".csv") {
				continue
			}
			filePath := fmt.Sprint(folderPath, "/", file.Name())
			readFile := io.ParseCsvFile(file.Name(), filePath, format)
			readFolder.AddFile(readFile)
		}
		folderArray = append(folderArray, readFolder)
	}
	return folderArray
}
