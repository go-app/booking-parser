package main

import (
	app "booking-parser/cmd/app"
)

func main() {
	app.Run()
}
