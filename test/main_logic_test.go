package test

import (
	app "booking-parser/cmd/app"
	"testing"
)

func TestRun(t *testing.T) {

	periodSumArr := app.RunWithConfig("./config.yaml")

	expectedLen := 1
	if len(periodSumArr) != expectedLen {
		t.Errorf("periodSum size is %v, want: %v.", len(periodSumArr), expectedLen)
	}

	expectPos := 1
	countPos := len(periodSumArr[0].Account.ReadFolder.ReadFiles[0].PosLines)
	if countPos != expectPos {
		t.Errorf("Expected positive bookings is %v, want: %v.", countPos, expectPos)
	}

	expectNeg := 6
	countNeg := len(periodSumArr[0].Account.ReadFolder.ReadFiles[0].NegLines)
	if countNeg != expectNeg {
		t.Errorf("Expected negative bookings is %v, want: %v.", countNeg, expectNeg)
	}
}

func TestViolations(t *testing.T) {

	periodSumArr := app.RunWithConfig("./config.yaml")

	if periodSumArr[0].Account.BookingPeriods[0].Violations.IsEmpty() {
		t.Errorf("Expect violations, but is empty")
	}

	expVio := 2
	vioCount := periodSumArr[0].Account.BookingPeriods[0].Violations.Size()
	if vioCount != expVio {
		t.Errorf("Expect %v violations, but got: %v", expVio, vioCount)
	}
}
