#!usr/bin/env bash

set euo pipefail

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo "Execute script here: $SCRIPTPATH"

(
    cd $SCRIPTPATH
    cd ..
    go run ./cmd/main.go ./example/config.yaml
)
