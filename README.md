# Parser for bookings
[![pipeline status](https://gitlab.com/go-app/booking-parser/badges/master/pipeline.svg)](https://gitlab.com/go-app/booking-parser/commits/master) 
[![coverage report](https://gitlab.com/go-app/booking-parser/badges/master/coverage.svg)](https://gitlab.com/go-app/booking-parser/commits/master)

If you have more than one bank account it would be nice to have an overview of all bookings.
To reach this you need to export your bookings as csv-file and parse it with this tool.

## Run example

With a script

```bash
cd booking-parser
bash ./example/run.sh
```
or with go

```bash
cd booking-parser
go run ./cmd/Main.go ./example/config.yaml
```

## Build executable

To build a executable we use docker-compose, so no need to install go.

```bash
docker-compose -f docker/build-version/docker-compose.yaml up
```

You will find the executable under `build-version/executable` and can run it in linux with

```bash
./docker/build-version/executable/booking-parser.sh
```

## Execute tests

```bash
docker-compose -f docker/build-version/docker-compose.yaml up
```